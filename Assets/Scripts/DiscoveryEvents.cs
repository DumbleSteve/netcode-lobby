

public static class DiscoveryEvents
{
	// lobby options

	public delegate void OnHostGameDelegate();
	public static OnHostGameDelegate OnHostGame;

	public delegate void OnJoinGameDelegate(string ipAddress);
	public static OnJoinGameDelegate OnJoinGame;

	public delegate void OnLeaveGameDelegate(ulong clientID);
	public static OnLeaveGameDelegate OnLeaveGame;

	// discovery

	public delegate void OnDiscoveryListenCountdownDelegate(int elapsedSeconds, int timeoutSeconds);
	public static OnDiscoveryListenCountdownDelegate OnDiscoveryListenCountdown;

	public delegate void OnDiscoveryListenTimeoutDelegate();
	public static OnDiscoveryListenTimeoutDelegate OnDiscoveryListenTimeout;

	public delegate void OnHostIPReceivedDelegate(string hostIP);
	public static OnHostIPReceivedDelegate OnHostIPReceived;

	// network players

	public delegate void OnNetworkPlayerEnteredGameDelegate(NetcodePlayer player);
	public static OnNetworkPlayerEnteredGameDelegate OnNetworkPlayerEnteredGame;

	public delegate void OnNetworkPlayerInitialisedDelegate(NetcodePlayer player);
	public static OnNetworkPlayerInitialisedDelegate OnNetworkPlayerInitialised;

	public delegate void OnNetworkPlayerLeftGameDelegate(ulong clientID);
	public static OnNetworkPlayerLeftGameDelegate OnNetworkPlayerLeftGame;

	// game status

	public delegate void OnNetcodeInitDelegate(int minPlayers, int maxPlayers);
	public static OnNetcodeInitDelegate OnNetcodeInit;

	public delegate void OnHostStartGameDelegate();
	public static OnHostStartGameDelegate OnHostStartGame;		// host clicked 'start game' button

	public delegate void OnGameStartedDelegate();
	public static OnGameStartedDelegate OnGameStarted;			// event fired via RPC to all clients when host starts game

	public delegate void OnHideLobbyDelegate(bool hide);
	public static OnHideLobbyDelegate OnHideLobby;              // when game starts

	// test

	public delegate void OnTestDelegate(int value);
	public static OnTestDelegate OnTest;
}
