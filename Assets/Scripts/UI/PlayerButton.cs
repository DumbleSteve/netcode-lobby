﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerButton : MonoBehaviour
{
    public NetcodePlayer NetworkPlayer { get; set; }

	public Button Button;
	public Image ButtonImage;
	public Image OwnerImage;			// local player
	public Image ColourImage;
	public Text PlayerNumber;
	public Text PlayerName;

    public ulong ClientID { get; private set; }


    private void OnEnable()
    {
		//TODO: action when button clicked? (eg. show stats)
    }

    private void OnDisable()
	{

    }

    public void SetPlayer(NetcodePlayer player, Color playerColour)
    {
		//Debug.Log($"SetPlayer colour {player.PlayerColour}");

		NetworkPlayer = player;
		ClientID = player.OwnerClientId;

		transform.SetSiblingIndex((int)player.OwnerClientId);           // order of buttons
		player.name = PlayerName.text = player.PlayerName;

		ButtonImage.color = playerColour;
		OwnerImage.enabled = player.IsOwner;
		ColourImage.color = player.PlayerColour;
	}

    public void PlayerLeftGame()
	{
		Button.interactable = false;
	}
}
