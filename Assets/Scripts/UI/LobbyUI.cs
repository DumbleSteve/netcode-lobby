﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


// wraps a UdpDiscovery instance with UI components to handle host IP send 
// and client receiving of host IP address, so clients can connect to the host
//
// effectively the 'lobby'

[DisallowMultipleComponent]
public class LobbyUI : MonoBehaviour
{
	public UdpDiscovery UdpDiscovery;

	public Button LobbyButton;
	public Image LobbyPanel;
	public CanvasGroup GameStartedPanel;

	private bool panelHidden;

	public Button HostGameButton;
	public Button JoinGameButton;
	public Button LeaveGameButton;
	public Button StartGameButton;

	public Text DiscoveryStatus;
	public Text DiscoveryCountdown;

	public GameObject NetworkPlayers;       // scroll view content
	public PlayerButton PlayerPrefab;       // for scroll view content

	private int minPlayers;              // min required to start game
	private int maxPlayers;
	public TextMeshProUGUI MinMaxPlayers;

	public Color LocalPlayerColour;
	public Color OtherPlayerColour;

	private NetcodePlayer localPlayer;

	private bool listeningforHost = false;      // so can cancel if no host found

	private bool internetReachable;
	private bool localNetworkReachable;

	private Vector2 lobbyPanelScale;
	private float lobbyScaleTime = 0.25f;

	private float gameStartedShowTime = 3f;
	private float gameStartedFadeTime = 1.5f;


	private void Awake()
    {
		lobbyPanelScale = LobbyPanel.transform.localScale;
		GameStartedPanel.gameObject.SetActive(false);
	}

    private void OnEnable()
	{
		internetReachable = (Application.internetReachability != NetworkReachability.NotReachable);
		localNetworkReachable = (Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork);

		ConfigNetworkButtons(false);
		StartGameButton.interactable = false;

		DiscoveryEvents.OnNetcodeInit += OnNetcodeInit;

		DiscoveryEvents.OnHostIPReceived += HostIPReceived;
		DiscoveryEvents.OnLeaveGame += OnLeaveGame;     // may be via client rpc when host leaves (all clients disconnected)

		DiscoveryEvents.OnGameStarted += OnGameStarted;     // host started game - UI notified via rpc

		DiscoveryEvents.OnNetworkPlayerEnteredGame += OnNetworkPlayerEnteredGame;
		DiscoveryEvents.OnNetworkPlayerInitialised += OnNetworkPlayerInitialised;
		DiscoveryEvents.OnNetworkPlayerLeftGame += OnNetworkPlayerLeftGame;
		DiscoveryEvents.OnDiscoveryListenCountdown += OnDiscoveryListenCountdown;
		DiscoveryEvents.OnDiscoveryListenTimeout += OnDiscoveryListenTimeout;

		LobbyButton.onClick.AddListener(OnLobbyButtonClicked);
		HostGameButton.onClick.AddListener(OnHostGameButtonClicked);
		JoinGameButton.onClick.AddListener(OnJoinGameButtonClicked);
		LeaveGameButton.onClick.AddListener(OnLeaveGameButtonClicked);
		StartGameButton.onClick.AddListener(OnStartGameButtonClicked);
	}

	private void OnDisable()
	{
		DiscoveryEvents.OnNetcodeInit -= OnNetcodeInit;

		DiscoveryEvents.OnHostIPReceived -= HostIPReceived;
		DiscoveryEvents.OnLeaveGame -= OnLeaveGame;

		DiscoveryEvents.OnGameStarted -= OnGameStarted;

		DiscoveryEvents.OnNetworkPlayerEnteredGame -= OnNetworkPlayerEnteredGame;
		DiscoveryEvents.OnNetworkPlayerInitialised -= OnNetworkPlayerInitialised;
		DiscoveryEvents.OnNetworkPlayerLeftGame -= OnNetworkPlayerLeftGame;
		DiscoveryEvents.OnDiscoveryListenCountdown -= OnDiscoveryListenCountdown;
		DiscoveryEvents.OnDiscoveryListenTimeout -= OnDiscoveryListenTimeout;

		LobbyButton.onClick.RemoveListener(OnLobbyButtonClicked);
		HostGameButton.onClick.RemoveListener(OnHostGameButtonClicked);
		JoinGameButton.onClick.RemoveListener(OnJoinGameButtonClicked);
		LeaveGameButton.onClick.RemoveListener(OnLeaveGameButtonClicked);
		StartGameButton.onClick.RemoveListener(OnStartGameButtonClicked);
	}

    private void OnLobbyButtonClicked()
	{
		Hide(!panelHidden);		// toggle lobby visibility
	}

	private void OnHostGameButtonClicked()
	{
		DisableNetworkButtons();
		BroadcastHostIP();

		DiscoveryEvents.OnHostGame?.Invoke();       // start host
	}

	// client
	private void OnJoinGameButtonClicked()
	{
		DisableNetworkButtons();
		StartDiscoveryListening();
	}

	private void OnLeaveGameButtonClicked()
	{
		DiscoveryEvents.OnLeaveGame?.Invoke(localPlayer.OwnerClientId);
	}

	private void OnLeaveGame(ulong clientID)
	{
		if (clientID == localPlayer.OwnerClientId)
		{
			DisableNetworkButtons();
			StopDiscoveryListening();
		}
	}

	// host can start when there are >= min players in lobby
	private void OnStartGameButtonClicked()
	{
		StopDiscoveryListening();
		ConfigNetworkButtons(true);     // StopDiscoveryListening can re-enable host button

		DiscoveryEvents.OnHostStartGame?.Invoke();
	}

	private void OnNetcodeInit(int minPlayers, int maxPlayers)
	{
		this.minPlayers = minPlayers;
		this.maxPlayers = maxPlayers;

		if (minPlayers <= 0 || maxPlayers <= 0 || minPlayers > maxPlayers)
		{
			MinMaxPlayers.color = Color.red;
			MinMaxPlayers.text = $"Min {minPlayers}\nMax {maxPlayers}!";
		}
		else
			MinMaxPlayers.text = $"{minPlayers}-{maxPlayers}";	
	}

	// lobby adds player button when new player enters game
	private void OnNetworkPlayerEnteredGame(NetcodePlayer player)
	{
		if (player.IsLocalPlayer)
		{
			ConfigNetworkButtons(true);
			localPlayer = player;
		}
	}

	private void OnNetworkPlayerInitialised(NetcodePlayer player)
	{
		if (GetPlayerButton(player.OwnerClientId) == null)      // don't want duplicate buttons! (eg. host/server)
		{
			//Debug.Log($"OnNetworkPlayerInitialised: ClientId = {player.OwnerClientId}");

			var playerButton = Instantiate(PlayerPrefab, NetworkPlayers.transform);     // scroll view content
			playerButton.SetPlayer(player, player.IsLocalPlayer ? LocalPlayerColour : OtherPlayerColour);

			ConfigStartGameButton(NetcodeManager.PlayerCount);
		}
	}

	private void OnNetworkPlayerLeftGame(ulong clientID)
	{
		// remove player button from scrollview
		// if the local player left the game, enable the host/join buttons
		var playerButton = GetPlayerButton(clientID);

		if (playerButton != null && playerButton.ClientID == clientID)
		{
			Destroy(playerButton.gameObject);

			if (playerButton.NetworkPlayer.IsLocalPlayer)
			{
				ConfigNetworkButtons(false);
			}
		}

		//Debug.Log($"OnNetworkPlayerLeftGame: clientID {clientID} PlayerCount {NetcodeManager.PlayerCount}");
		ConfigStartGameButton(NetcodeManager.PlayerCount - 1);      // departing player still in SpawnManager.SpawnedObjectsList
	}

	private PlayerButton GetPlayerButton(ulong clientID)
	{
		foreach (Transform playerTransform in NetworkPlayers.transform)
		{
			var playerButton = playerTransform.GetComponent<PlayerButton>();

			if (playerButton.ClientID == clientID)
				return playerButton;
		}

		return null;
	}

	private void OnDiscoveryListenCountdown(int elapsedSeconds, int timeoutSeconds)
	{
		DiscoveryCountdown.text = (timeoutSeconds - elapsedSeconds).ToString();
	}

	private void OnDiscoveryListenTimeout()
	{
		StopDiscoveryListening();
		EnableNetworkButtons();
	}

	private void OnGameStarted()
	{
		Hide(true);
		ShowGameStartedPanel();
	}

	private void ShowGameStartedPanel()
	{
		GameStartedPanel.gameObject.SetActive(true);

		LeanTween.value(1f, 0f, gameStartedFadeTime)
					.setDelay(gameStartedShowTime)
					.setOnUpdate((float f) => GameStartedPanel.alpha = f)
					.setOnComplete(() => GameStartedPanel.gameObject.SetActive(false))
					.setEaseOutSine();
	}

	private void DisableNetworkButtons()
	{
		HostGameButton.interactable = false;
		JoinGameButton.interactable = false;
		LeaveGameButton.interactable = false;
	}

	private void EnableNetworkButtons()
	{
		HostGameButton.interactable = true;
		JoinGameButton.interactable = true;
		LeaveGameButton.interactable = true;
	}

	private void ConfigNetworkButtons(bool inGame)
	{
		HostGameButton.interactable = localNetworkReachable && !inGame;
		JoinGameButton.interactable = localNetworkReachable && !inGame; // && NetcodeManager.PlayerCount < maxPlayers;
		LeaveGameButton.interactable = localNetworkReachable && inGame;
	}

	private void ConfigHostButton()
	{
		HostGameButton.interactable = localNetworkReachable && !listeningforHost;
	}

	private void ConfigStartGameButton(int playerCount)
	{
		//Debug.Log($"ConfigStartGameButton: playerCount {playerCount} isHost {localPlayer.PlayerIsHost}");
		StartGameButton.interactable = (playerCount >= minPlayers && localPlayer != null && localPlayer.PlayerIsHost);
	}

	private void Hide(bool hide)
	{
		if (panelHidden == hide)
			return;

		LeanTween.scaleY(LobbyPanel.gameObject, (hide ? 0f : lobbyPanelScale.y), lobbyScaleTime)
					.setEase(hide ? LeanTweenType.easeInQuad : LeanTweenType.easeOutBack)
					.setOnComplete(() => panelHidden = hide);
	}

	private void HostIPReceived(string hostIP)
	{
		//Debug.Log("LobbyUI.HostIPReceived: " + hostIP);

		// join game immediately host ip received
		DiscoveryEvents.OnJoinGame?.Invoke(hostIP);     // start client, using host IP address to connect via UnityTransport

		StopDiscoveryListening();                  // stops listening and broadcasting
		ConfigHostButton();
	}

	private void StartDiscoveryListening()
	{
		//Debug.Log("StartDiscoveryListening");

		DiscoverHostIP();      // listen as client to host broadcasts 
		listeningforHost = true;

		ConfigHostButton();                 // can't host if listening for host broadcast
	}

	private void StopDiscoveryListening()
	{
		//Debug.Log("StopDiscoveryListening");

		StopDiscovery();       // stops listening and broadcasting
		listeningforHost = false;

		ConfigHostButton();
	}

	// broadcast for client to discover
	private void BroadcastHostIP()
	{
		//Debug.Log("BroadcastHostIP Discovery.StartAsServer");

		UdpDiscovery.StartBroadcasting();
		DiscoveryStatus.text = "Broadcasting...";
	}

	// start listening for host IP broadcast
	private void DiscoverHostIP()
	{
		//Debug.Log("DiscoverHostIP Discovery.StartAsClient");

		UdpDiscovery.StartListening();
		DiscoveryStatus.text = "Listening...";
	}

	private void StopDiscovery()
	{
		UdpDiscovery.StopBroadcasting();
		UdpDiscovery.StopListening();

		DiscoveryStatus.text = "";
		DiscoveryCountdown.text = "";
	}
}
