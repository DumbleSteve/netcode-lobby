using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TestUI : MonoBehaviour
{
    public Button test1Button;
    public Button test2Button;
    public TextMeshProUGUI testValue;

    private void OnEnable()
    {
        test1Button.onClick.AddListener(Test1);
        test2Button.onClick.AddListener(Test2);
    }

    private void OnDisable()
    {
        test1Button.onClick.RemoveListener(Test1);
        test2Button.onClick.RemoveListener(Test2);
    }

    private void Test1()
    {
        DiscoveryEvents.OnTest?.Invoke(1);
        testValue.text = "1";
    }

    private void Test2()
    {
        DiscoveryEvents.OnTest?.Invoke(2);
        testValue.text = "2";
    }
}
