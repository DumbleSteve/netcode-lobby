﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Unity.Netcode;

using Unity.Netcode.Transports.UTP;     // Unity Transport Protocal as used by NetworkManager

[DisallowMultipleComponent]
[RequireComponent(typeof(UnityTransport))]

// class to handle starting and stopping hosts and clients (ie. connecting NetcodePlayers to NetworkManager)

public class NetcodeManager : MonoBehaviour
{
    public UdpDiscovery UdpDiscovery;               // to get local IP address of host for UDP broadcast
    //public string RoomPassword = "Discovery";     // for connection approval (not currently used)

    public int MinPlayers = 2;                     // min required to start game
    public int MaxPlayers = 4;                     // min required to start game

    public List<Color> PlayerColours = new();       // set in inspector

    private UnityTransport unityTransport;
    private const ushort PortNumber = 7777;

    public static Dictionary<ulong, NetworkObject> SpawnedPlayersDict => NetworkManager.Singleton.SpawnManager.SpawnedObjects;

    public static List<NetcodePlayer> NetworkPlayers
    {
        get
        {
            List<NetcodePlayer> players = new();

            foreach (var spawnedPlayer in NetworkManager.Singleton.SpawnManager.SpawnedObjectsList)
            {
                players.Add(spawnedPlayer.GetComponent<NetcodePlayer>());
            }
            return players;
        }
    }

    public static int PlayerCount { get { return NetworkManager.Singleton.SpawnManager.SpawnedObjectsList != null ?
                                                        NetworkManager.Singleton.SpawnManager.SpawnedObjectsList.Count : 0; } }

    public static bool HasPlayers { get { return PlayerCount > 0; } }

    public static NetcodePlayer GameHost { get { return NetworkPlayers.Where(x => x.PlayerIsHost).FirstOrDefault(); } }

    public static NetworkObject GetPlayerByClientId(ulong clientId) => HasPlayers ? NetworkManager.Singleton.SpawnManager.GetPlayerNetworkObject(clientId) : null;
    public static NetcodePlayer GetLocalPlayer => NetworkManager.Singleton.SpawnManager.GetLocalPlayerObject().GetComponent<NetcodePlayer>();
    public static NetcodePlayer GetPlayerByNumber(int playerNumber) => NetworkPlayers.Where(x => x.PlayerNumber == playerNumber).FirstOrDefault();

    // NetworkClient contains player object, owned objects, cryptography keys and more
    public static NetworkClient GetClient(ulong clientId) => HasPlayers ? NetworkManager.Singleton.ConnectedClients[clientId] : null;

    private float playerSpacing = 2f;       // TODO: temp solution to keep spawned players apart


    private void OnEnable()
    {
        DiscoveryEvents.OnHostGame += OnHostGame;
        DiscoveryEvents.OnJoinGame += OnJoinGame;
        DiscoveryEvents.OnLeaveGame += OnLeaveGame;

        DiscoveryEvents.OnNetworkPlayerEnteredGame += OnNetworkPlayerEnteredGame;
    }

    private void OnDisable()
    {
        DiscoveryEvents.OnHostGame -= OnHostGame;
        DiscoveryEvents.OnJoinGame -= OnJoinGame;
        DiscoveryEvents.OnLeaveGame -= OnLeaveGame;

        DiscoveryEvents.OnNetworkPlayerEnteredGame -= OnNetworkPlayerEnteredGame;
    }

    private void Start()
    {
        unityTransport = GetComponent<UnityTransport>();

        NetworkManager.Singleton.NetworkConfig.ConnectionApproval = false;      // see ApprovalCheck below - checks max player limit

        if (MinPlayers <= 0 || MaxPlayers <=0 || MinPlayers > MaxPlayers)
            Debug.LogError($"NetcodeManager: Invalid player count! (min {MinPlayers} max {MaxPlayers}");

        DiscoveryEvents.OnNetcodeInit?.Invoke(MinPlayers, MaxPlayers);
    }

    private void OnHostGame()
    {
        StartHost();
    }

    private void StartHost(ushort port = PortNumber)
    {
        if (NetworkManager.Singleton.IsListening == true)
            return;

        try
        {
            unityTransport.ConnectionData.Address = UdpDiscovery.LocalIPAddress;
        }
        catch (Exception ex)
        {
            Debug.LogError("StartHost: unable to resolve local IP address: " + ex.Message);
            return;
        }

        unityTransport.ConnectionData.Port = port;

        NetworkManager.Singleton.OnClientDisconnectCallback += OnClientDisconnectCallback;
        NetworkManager.Singleton.ConnectionApprovalCallback = ApprovalCheck;            // denies approval if MaxPlayers limit reached

        //Debug.Log("StartHost: Host IP = " + unityTransport.ConnectionData.Address + " port = " + unityTransport.ConnectionData.Port);
        NetworkManager.Singleton.StartHost();        // spawns a NetworkPlayer
    }


    private void OnJoinGame(string hostIPAddress)
    {
        // password for connection approval... (not used)
        //NetworkManager.Singleton.NetworkConfig.ConnectionData = Encoding.ASCII.GetBytes(RoomPassword);

        try
        {
            StartClient(hostIPAddress);
        }
        catch (Exception ex)
        {
            Debug.LogError("OnJoinGame Exception: " + ex + " Message: " + ex.Message);
        }
    }

    private void StartClient(string hostIP, ushort port = PortNumber)
    {
        if (NetworkManager.Singleton.IsListening == true)
            return;

        unityTransport.ConnectionData.Address = hostIP;
        unityTransport.ConnectionData.Port = port;

        //Debug.Log("StartClient: IP = " + unityTransport.ConnectionData.Address + " port = " + unityTransport.ConnectionData.Port);
        NetworkManager.Singleton.StartClient();      // spawns a NetworkPlayer
    }


    private void OnLeaveGame(ulong clientID)
    {
        ulong localClientId = NetworkManager.Singleton.LocalClientId;

        if (GetLocalPlayer != null)
        {
            if (clientID == localClientId)
            {
                //Debug.Log("OnLeaveGame: OwnerClientId = " + localClientId);
                Disconnect();
            }
        }
    }

    // initialise new player (number, name, colour, etc.)
    private void OnNetworkPlayerEnteredGame(NetcodePlayer player)
    {
        int playerNumber = player.SetPlayerNumber();
        Color playerColor = Color.white;

        if (PlayerColours.Count > 0)
            playerColor = (PlayerColours.Count >= playerNumber) ? PlayerColours[playerNumber - 1] : PlayerColours[0];

        player.SetPlayerColour(playerColor);

        // TODO: probably remove this - temp to keep player objects apart on spawning
        player.transform.position = new Vector3((playerNumber - 1) * playerSpacing, 1f, 0f);

        DiscoveryEvents.OnNetworkPlayerInitialised?.Invoke(player);

        //Debug.Log($"OnNetworkPlayerEnteredGame: PlayerNumber {player.PlayerNumber}");
    }

    private void Disconnect()
    {
        NetworkManager.Singleton.Shutdown();
    }


    // callback assigned to NetworkManager.Singleton.ConnectionApprovalCallback
    // called if NetworkManager.Singleton.NetworkConfig.ConnectionApproval is set to true
    // denies approval if MaxPlayers limit reached
    private void ApprovalCheck(NetworkManager.ConnectionApprovalRequest request, NetworkManager.ConnectionApprovalResponse response)
    {
        // The client identifier to be authenticated
        var clientId = request.ClientNetworkId;

        // Additional connection data defined by user code
        var connectionData = request.Payload;

        // Your approval logic determines the following values
        response.Approved = PlayerCount < MaxPlayers;        // room for at least one more!
        response.CreatePlayerObject = response.Approved;

        // The Prefab hash value of the NetworkPrefab, if null the default NetworkManager player Prefab is used
        response.PlayerPrefabHash = null;

        // Position to spawn the player object (if null it uses default of Vector3.zero)
        response.Position = Vector3.zero;

        // Rotation to spawn the player object (if null it uses the default of Quaternion.identity)
        response.Rotation = Quaternion.identity;

        //// If response.Approved is false, you can provide a message that explains the reason why via ConnectionApprovalResponse.Reason
        //// On the client-side, NetworkManager.DisconnectReason will be populated with this message via DisconnectReasonMessage
        // response.Reason = "Game player limit reached!!";

        // If additional approval steps are needed, set this to true until the additional steps are complete
        // once it transitions from true to false the connection approval response will be processed.
        response.Pending = false;
    }

    private void OnClientDisconnectCallback(ulong obj)
    {
        if (NetworkManager.Singleton.IsServer)
        {
            Debug.Log($"Approval Declined! Player limit reached.");
        }
    }
}
