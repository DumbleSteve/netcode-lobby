using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Netcode;
using UnityEngine;


public class NetcodePlayer : NetworkBehaviour
{
    [SerializeField] private int testValue;
    //private float moveSpeed = 3f;

    //private NetworkVariable<int> networkInt;
    //private NetworkVariable<float> networkFloat;
    //private NetworkVariable<bool> networkBool;

    ////create an INetworkSerializable struct that can be used as a variable that is synced across the network value types only!
    //private NetworkVariable<MyCustomData> randomNetworkData = new NetworkVariable<MyCustomData>(
    //    new MyCustomData
    //    {
    //        _int = 56,
    //        _bool = true,
    //        _message = "",
    //    }, NetworkVariableReadPermission.Everyone, NetworkVariableWritePermission.Owner);       // server can always write

    public bool PlayerIsHost => IsHost;         // protected variable
    public bool PlayerIsClient => IsClient;     // protected variable

    public int PlayerNumber { get; private set; }
    public Color PlayerColour { get; private set; }

    public string PlayerName => $"Player {NetcodeManager.PlayerCount}";

    private Renderer playerRenderer;

    private void Awake()
    {
        playerRenderer = GetComponent<Renderer>();
    }

    // NOTE: use this instead of Start or Awake for NetworkBehaviours!
    public override void OnNetworkSpawn()
    {
        if (IsOwner && IsHost)
            DiscoveryEvents.OnHostStartGame += OnHostStartGame;		// host starts game - UI notified via rpc

        //Debug.Log($"<color=lightblue>NetworkPlayer.OnNetworkSpawn: '{name}' OwnerClientId {OwnerClientId} IsHost {IsHost}  IsOwner {IsOwner} </color>");

        if (IsHost)
            NetworkManager.Singleton.OnClientDisconnectCallback += OnClientDisconnected;

        if (IsOwner)
            DiscoveryEvents.OnTest += OnTest;

        DiscoveryEvents.OnNetworkPlayerEnteredGame?.Invoke(this);      // eg. add to UI / reactivate buttons etc.
    }

    public override void OnNetworkDespawn()
    {
        if (IsOwner && IsHost)
            DiscoveryEvents.OnHostStartGame -= OnHostStartGame;     // host starts game - notify UI via rpc

        if (IsOwner)
            DiscoveryEvents.OnTest -= OnTest;

        DiscoveryEvents.OnNetworkPlayerLeftGame?.Invoke(OwnerClientId);      // eg. add to UI / reactivate buttons etc.
    }

    // owner only
    private void OnTest(int value)
    {
        //Debug.Log("Host OnTest " + value);
        testValue = value;
        OnTestServerRpc(value);
    }

    [ServerRpc]
    private void OnTestServerRpc(int value)
    {
    }

    private void OnHostStartGame()
    {
        if (IsOwner && IsHost)
        {
            HostStartGameClientRPC();       // fire OnGameStarted event on all clients
        }
    }

    // response to NetworkManager client disconnected event
    // if host left, disconnect all clients
    private void OnClientDisconnected(ulong clientID)
    {
        try
        {
            if (IsHost)
            {
                HostLeftGameClientRPC(clientID);
                //Debug.Log("Host OnClientDisconnected " + clientID);
            }
        }
        catch
        {
            // NetworkPlayer may already have been destroyed (eg. by host leaving)
            Debug.Log("NetworkPlayer.OnClientDisconnected NetworkPlayer for clientID " + clientID + " already destroyed!");
        }
    }

    [ClientRpc]
    public void HostStartGameClientRPC()
    {
        //Debug.Log("HostStartGameClientRPC ");
        DiscoveryEvents.OnGameStarted?.Invoke();      // eg. hide lobby, start play
    }

    [ClientRpc]
    public void HostLeftGameClientRPC(ulong clientID)
    {
        //Debug.Log("HostLeftGameClientRPC " + clientID);
        DiscoveryEvents.OnLeaveGame?.Invoke(clientID);      // eg. remove from UI / reactivate buttons etc.
    }

    public int SetPlayerNumber()
    {
        PlayerNumber = NetcodeManager.PlayerCount;
        return PlayerNumber;
    }

    public void SetPlayerColour(Color colour)
    {
        //Debug.Log($"SetPlayerColour colour {colour}");
        PlayerColour = colour;

        if (playerRenderer != null)
            playerRenderer.material.color = colour;
    }

    //// move player with WASD keys
    //// set random number with T key
    //private void Update()
    //{
    //    if (!IsOwner)
    //        return;

    //    if (Input.GetKeyDown(KeyCode.T))
    //    {
    //        // spawn network object on all clients
    //        //spawnedNetworkObj = Instantiate(networkObjectPrefab);
    //        //spawnedNetworkObj.GetComponent<NetworkObject>().Spawn(true);      // destroy with scene, which is default for regular objects

    //        //randomData.Value = new MyCustomData {
    //        //    _int = Random.Range(0, 100),
    //        //    _bool = false,
    //        //    _message = "Random string" 
    //        //};

    //        //TestServerRpc();
    //        //TestClientRpc();
    //    }

    //    if (Input.GetKeyDown(KeyCode.Y))
    //    {
    //        //Destroy(spawnedNetworkObj.gameObject);      // destroyed on all clients
    //    }

    //    Vector3 moveDir = new();

    //    if (Input.GetKey(KeyCode.W)) moveDir.z = 1f; 
    //    if (Input.GetKey(KeyCode.S)) moveDir.z = -1f; 
    //    if (Input.GetKey(KeyCode.A)) moveDir.x = -1f; 
    //    if (Input.GetKey(KeyCode.D)) moveDir.x = 1f;

    //    transform.position += moveSpeed * Time.deltaTime * moveDir;
    //}


    //// struct (value type) that can be used as a NetworkVariable, as long as it contains value types only
    //public struct MyCustomData : INetworkSerializable
    //{
    //    public int _int;
    //    public bool _bool;
    //    public FixedString128Bytes _message;        // regular string not serializable (not a value type)

    //    public void NetworkSerialize<T>(BufferSerializer<T> serializer) where T : IReaderWriter
    //    {
    //        serializer.SerializeValue(ref _int);
    //        serializer.SerializeValue(ref _bool);
    //        serializer.SerializeValue(ref _message);
    //    }
    //}
}
