﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using UnityEngine;


// UDP broadcast and listen utility
// for game host to broadcast IP address for clients
// to recieve and connect to using UNetTransport

public class UdpDiscovery : MonoBehaviour
{
    public float BroadcastInterval = 1f;
	public float ListenInterval = 1f;
	public float ListenTimeOut = 15f;

    public int BroadcastPort = 47777;
    public int ListenPort = 47777;

    private Coroutine broadcastCoroutine;
    private bool broadcasting = false;

    private Coroutine listenCoroutine;
    private bool listening = false;
    private DateTime listenStartTime;

    private UdpClient udpServer;		// broadcast
    private UdpClient udpClient;		// listen


	#region broadcasting

	// send out host ip address
	public void StartBroadcasting()
    {
        StopBroadcasting();
        broadcastCoroutine = StartCoroutine(Broadcast());
    }

    private IEnumerator Broadcast()
    {
        if (broadcasting)
            yield break;

        string ipAddress = "";      // converted to Byte[] below

        try
        {
            ipAddress = LocalIPAddress;
            //Debug.Log($"Broadcast: ipAddress = {ipAddress}");

            if (ipAddress == null)
            {
                Debug.LogError("null LocalIPAddress!");
                yield break;
            }
		}
		catch (Exception ex)
		{
			Debug.LogError("Broadcast: failed to resolve LocalIPAddress: " + ex.Message);
            yield break;
        }

		byte[] ipBytes = Encoding.ASCII.GetBytes(ipAddress);
		broadcasting = true;

        udpServer = new UdpClient
        {
            EnableBroadcast = true
        };

        while (broadcasting)
        {
			udpServer.Send(ipBytes, ipBytes.Length, new IPEndPoint(IPAddress.Broadcast, BroadcastPort));

			//Debug.Log("Broadcasting: " + Encoding.ASCII.GetString(ipBytes));
            yield return new WaitForSeconds(BroadcastInterval);
        }

        udpServer.Close();
		yield return null;
    }

    public void StopBroadcasting()
    {
        if (broadcastCoroutine != null)
        {
            StopCoroutine(broadcastCoroutine);
            broadcastCoroutine = null;
        }

        broadcasting = false;
		if (udpServer != null)
			udpServer.Close();
    }

    #endregion


    #region listening

    // listen for (receive) host ip address for NetworkPlayer to be spawned (MLAPI) - see OnHostIP event
    public void StartListening()
    {
        StopListening();
        listenCoroutine = StartCoroutine(Listen());
    }

    private IEnumerator Listen()
	{
        listening = true;
        listenStartTime = DateTime.Now;

        double elapsedSeconds = 0;

        udpClient = new UdpClient();
        udpClient.EnableBroadcast = true;
        udpClient.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true); // allows connections within the computer
        udpClient.Client.Bind(new IPEndPoint(IPAddress.Any, ListenPort));       // associate socket with a new endpoint

        udpClient.BeginReceive(new AsyncCallback(ReceiveCallback), null);       // async - doesn't block

        // timeout if waiting too long for EndReceive...
        while (listening)
		{
            elapsedSeconds = (DateTime.Now - listenStartTime).TotalSeconds;
            //DiscoveryEvents.OnDiscoveryListenCountdown?.Invoke((int)elapsedSeconds, (int)ListenTimeOut);

            if (elapsedSeconds >= ListenTimeOut)
            {
                StopListening();
                DiscoveryEvents.OnDiscoveryListenTimeout?.Invoke();
				yield break;
            }

            yield return new WaitForSecondsRealtime(ListenInterval);
            DiscoveryEvents.OnDiscoveryListenCountdown?.Invoke((int)elapsedSeconds, (int)ListenTimeOut);
        }

        yield return null;
	}

    // async callback from BeginReceive() - ie. host IP address received
    // Dispatcher used to return to main thread to fire OnHostIPReceived and stop listening
    private void ReceiveCallback(IAsyncResult result)
    {
        try
        {
            IPEndPoint receiveEndPoint = new IPEndPoint(IPAddress.Any, ListenPort);
            byte[] ipAddress = udpClient.EndReceive(result, ref receiveEndPoint);

            //Debug.Log("EndReceive ipAddress: " + Encoding.ASCII.GetString(ipAddress));

			// dispatch OnHostIp event invocation back to main thread
            Dispatcher.Invoke(() =>
            {
                DiscoveryEvents.OnHostIPReceived?.Invoke(Encoding.ASCII.GetString(ipAddress));
                StopListening();          // stop listening once an ip address has been received
            });
        }
        catch
		{
            // callback called as a result of udpClient.Close() (via timeout) will throw exception...
            // (udpClient.Close() is only way to cancel the BeginReceive)
        }
	}

    public void StopListening()
	{
        listening = false;

        if (listenCoroutine != null)
        {
            StopCoroutine(listenCoroutine);
            listenCoroutine = null;
        }

        try
        {
			if (udpClient != null)
				udpClient.Close();
        }
        catch
        {
            // in case udpClient spits the dummy...
        }
	}

	#endregion


    // utility function to get local IP address by connecting
    // to a socket and getting the IP from the resulting end point
	public string LocalIPAddress
    {
        get
        {
            if (!System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
            {
                Debug.Log("LocalIPAddress GetIsNetworkAvailable = false");
                return null;
            }

            string localIP;
            using (Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, 0))
            {
                socket.Connect("8.8.8.8", BroadcastPort);
                IPEndPoint endPoint = socket.LocalEndPoint as IPEndPoint;
                localIP = endPoint.Address.ToString();
                //Debug.Log($"LocalIPAddress localIP = {localIP}");
            }

            return localIP;
        }
    }
}
